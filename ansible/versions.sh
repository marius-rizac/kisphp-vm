#!/usr/bin/env bash

PHP_VERSION=$(php -v | head -1 | cut -d' ' -f2)
MYSQL_VERSION=$(mysql -V | cut -d' ' -f6)
NODEJS_VERSION=$(node -v)
NPM_VERSION=$(npm -v)
JAVA_VERSION=$(java -version 2>&1 | head -1 | cut -d' ' -f3)
ES_VERSION=$(curl -XGET 'localhost:9200' 2>&1 | grep number | cut -d'"' -f4)
LUCENE_VERSION=$(curl -XGET 'localhost:9200' 2>&1 | grep lucene_version | cut -d'"' -f4)

echo "PHP: ${PHP_VERSION}"
echo "MySQL: ${MYSQL_VERSION}"
echo "NodeJS: ${NODEJS_VERSION}"
echo "NPM: ${NPM_VERSION}"
echo "Redis: $(redis-cli -v)"
echo "GIT: $(git --version)"
echo "JAVA: ${JAVA_VERSION//\"}"
echo "ELASTICSEARCH: ${ES_VERSION}"
echo "Lucene: ${LUCENE_VERSION}"
