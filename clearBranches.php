<?php

class Arguments
{
    /**
     * @var self
     */
    protected static $instance;

    /**
     * @var array
     */
    protected $argv = [];

    protected function __construct()
    {

    }

    /**
     * @param array $argv
     * @return $this
     */
    public function setArgv(array $argv)
    {
        $this->argv = $argv;

        return $this;
    }

    /**
     * @param $argument
     * @return bool
     */
    public function hasArgument($argument)
    {
        return (in_array($argument, $this->argv));
    }

    /**
     * @return Arguments
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }
}

class ColorText
{
    protected $foreground_colors = [];
    protected $background_colors = [];

    public function __construct()
    {
        $this->foreground_colors['black'] = '0;30';
        $this->foreground_colors['dark_gray'] = '1;30';
        $this->foreground_colors['blue'] = '0;34';
        $this->foreground_colors['light_blue'] = '1;34';
        $this->foreground_colors['green'] = '0;32';
        $this->foreground_colors['light_green'] = '1;32';
        $this->foreground_colors['cyan'] = '0;36';
        $this->foreground_colors['light_cyan'] = '1;36';
        $this->foreground_colors['red'] = '0;31';
        $this->foreground_colors['light_red'] = '1;31';
        $this->foreground_colors['purple'] = '0;35';
        $this->foreground_colors['light_purple'] = '1;35';
        $this->foreground_colors['brown'] = '0;33';
        $this->foreground_colors['yellow'] = '1;33';
        $this->foreground_colors['light_gray'] = '0;37';
        $this->foreground_colors['white'] = '1;37';

        $this->background_colors['black'] = '40';
        $this->background_colors['red'] = '41';
        $this->background_colors['green'] = '42';
        $this->background_colors['yellow'] = '43';
        $this->background_colors['blue'] = '44';
        $this->background_colors['magenta'] = '45';
        $this->background_colors['cyan'] = '46';
        $this->background_colors['light_gray'] = '47';
    }

    /**
     * @param string $colorName
     *
     * @return string
     */
    protected function getColor($colorName)
    {
        return "\033[" . $this->foreground_colors[$colorName] . "m";
    }

    /**
     * @param bool $endNewLine
     *
     * @return string
     */
    protected function getColorEnd($endNewLine = true)
    {
        $endLine = ($endNewLine === true) ? "\n" : '';

        return "\033[0m" . $endLine;
    }

    /**
     * @param string $text
     * @param bool $endNewLine
     *
     * @return string
     */
    public function info($text, $endNewLine = true)
    {
        return $this->getColor('light_blue') . $text . $this->getColorEnd($endNewLine);
    }

    /**
     * @param string $text
     * @param bool $endNewLine
     *
     * @return string
     */
    public function warning($text, $endNewLine = true)
    {
        return $this->getColor('red') . $text . $this->getColorEnd($endNewLine);
    }

    /**
     * @param string $text
     * @param bool $endNewLine
     *
     * @return string
     */
    public function success($text, $endNewLine = true)
    {
        return $this->getColor('green') . $text . $this->getColorEnd($endNewLine);
    }
}

class ClearBranches
{
    /**
     * @var array
     */
    protected $protectedBranches = [
        'develop',
        'master',
    ];

    /**
     * @var array
     */
    protected $localBranches = [];

    /**
     * @var array
     */
    protected $remoteBranches = [];

    /**
     * @var ColorText
     */
    protected $colorText;

    /**
     * @param ColorText $colorText
     */
    protected function __construct(ColorText $colorText)
    {
        $this->colorText = $colorText;

        $this->gitFetch();
        $this->getLocalBranches();
        $this->getRemoveBranches();
    }

    /**
     * @return ClearBranches
     */
    public static function create(ColorText $colorText)
    {
        return new self($colorText);
    }

    /**
     * @param string $branch
     * @return bool
     */
    protected function displayOrExecute($branch)
    {
        if (Arguments::getInstance()->hasArgument('-f')) {
            echo $this->colorText->success('Remove branch: ', false) . $this->colorText->warning($branch);
            exec("git branch -D " . $branch);

            return true;
        }

        echo "git branch -D " . $this->colorText->warning($branch);

        return true;
    }

    private function gitFetch()
    {
        echo $this->colorText->info('Git fetch');
        exec('git fetch -p');
    }

    private function getLocalBranches()
    {
        $localBranches = [];
        exec('git branch', $localBranches);
        $this->localBranches = $this->filterBranches($localBranches);
    }

    private function getRemoveBranches()
    {
        $remoteBranches = [];
        exec('git branch -r', $remoteBranches);
        $this->remoteBranches = $this->filterBranches($remoteBranches, true);
    }

    /**
     * @param array $branches
     * @param bool|false $isRemote
     * @return array
     */
    private function filterBranches(array $branches, $isRemote = false)
    {
        $filteredBranches = [];
        foreach ($branches as $branch) {
            if (!$isRemote && preg_match('/\*/', $branch)) {
                continue;
            }

            if (!$isRemote && in_array(trim($branch), $this->protectedBranches)) {
                continue;
            }

            $filteredBranches[] = str_replace('origin/', '', trim($branch));
        }

        return $filteredBranches;
    }

    public function run()
    {
        echo $this->colorText->info('Branches to remove');
        $branchesToRemove = [];
        foreach ($this->localBranches as $localBranch) {
            if (!in_array($localBranch, $this->remoteBranches)) {
                $branchesToRemove[] = $localBranch;
            }
        }
        if (count($branchesToRemove) < 1) {
            echo $this->colorText->success('No orphan branches! Well done!');

            return true;
        }

        foreach ($branchesToRemove as $branch) {
            $this->displayOrExecute($branch);
        }

        return true;
    }
}

$fileArguments = Arguments::getInstance()->setArgv($argv);

ClearBranches::create(new ColorText())->run();
