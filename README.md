# Vagrant + VirtualBox Ansible setup

### Requirements
- Vagrant 2.2.0+
- Ruby 2.6+
- Virtual Box 6.0 [Get it from here for MAC](http://download.virtualbox.org/virtualbox/5.1.26/VirtualBox-5.1.26-117224-OSX.dmg)
- Ansible 5.3+ [Follow Instructions here](http://docs.ansible.com/ansible/latest/intro_installation.html#latest-releases-via-pip)
- vbguest plugin for vagrant. Run this command: 

### Install vagrant plugin
```
vagrant plugin install vagrant-vbguest
```

## Installation

```bash
# create virtual box machine
vagrant up

# run ansible configuration
./setup-local.sh
```

Inside VM, you may use shortcut `sf` instead of `vendor/bin/console`

Add the following lines to your `/etc/hosts` file:

> Note! If you change the IP in Vagrantfile make sure you change it here too.

```bash
10.10.5.61 dev.local api.local
```

### Shortcuts

The virtual server will have a tool that provides useful aliases:

Aliases from [Dotfiles](https://github.com/kisphp/dotfiles)

- `sf` -> shortcut to symfony console
- `uncommit` -> cancel last commit (if it was not pushed yet)
- `mcd` -> shortcut to `mkdir <dir_name> && cd <dir_name>`
- `ngre` -> restart nginx
- `phpre` -> restart php-fpm
- `myre` -> restart mysql
- `ngre` -> restart nginx
- `gign` -> add file or directory to .gitignore file (you must be in root directory of the project)
- `makeup` -> quick git add/commit/push. See full description [here](https://github.com/kisphp/dotfiles/blob/master/docs/makeup.md)

## Execute Ansible manually

```bash
cd ansible

ansible-playbook -i vagrant.ini setup-vagrant.yml -u ubuntu
```

## Execute only specific roles/tasks defined by tags

```bash
# Remove hosts defined in NGINX
ansible-playbook -i vagrant.ini setup-vagrant.yml -u ubuntu --tags project-clean

# Create NGINX project virtual hosts
ansible-playbook -i vagrant.ini setup-vagrant.yml -u ubuntu --tags project
```

## List all tasks available for your playbook

With the following command you can see what tasks will run and which tags they have assigned.

```bash
ansible-playbook -i vagrant.ini setup-vagrant.yml -u ubuntu --list-tasks
```

## List all tags available for your playbook

With the following command you can see what tags you have available for your tasks

```bash
ansible-playbook -i vagrant.ini setup-vagrant.yml -u ubuntu --list-tasks

# You may also list all tags that will be executed by the tag selection that you make
ansible-playbook -i vagrant.ini setup-vagrant.yml -u ubuntu --tags project --list-tasks
```

> Available tags:
> apt, composer, crons, dotfiles, git, hosts, mysql, mysql-install, mysql-users, nginx, nodejs, ntp, ohmyzsh, php7, postfix, project, project-clean, project-clone, python, server-manager, ssh, xdebug7
