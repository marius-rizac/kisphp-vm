#!/usr/bin/env bash

if [[ ! -f ansible/config.yml ]]; then
    cp ansible/config.dist.yml ansible/config.yml
fi

ansible-playbook -i ansible/vagrant.ini ansible/setup-vagrant.yml -u vagrant $@
